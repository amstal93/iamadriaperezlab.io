const updateHeader = () => {
    setScrollingHeader(getRelativeSocialIconsPosition() < 0);
}

const getViewportHeigh = () => {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 1);
}
const getSocialIconsPosition = () => {
    return document.querySelector('.title > ul').getBoundingClientRect().bottom;
}

const getRelativeSocialIconsPosition = () => {
    return getSocialIconsPosition() / getViewportHeigh() * 100;
}

const setScrollingHeader = (active) => {
    document.querySelectorAll('.header-social').forEach(element => {
        if (active) {
            element.classList.remove('hidden');
        } else {
            element.classList.add('hidden');
        }
    });
}

window.addEventListener('scroll', updateHeader);
window.addEventListener('rezise', updateHeader);
